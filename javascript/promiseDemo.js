

let p=document.getElementById("demo");
let flag=true;
let promise=new Promise((resolve,reject)=>{
    if(flag){
        resolve("operation was completed successfully");
    }
    else{
        reject("operation failed");
    }
})

promise.then(data=>{
    alert(data);
    p.innerText=data;
}).catch(err=>{

    alert(err);
    p.innerText=err;

});


console.log(promise);
console.log("3");

//asynchronous code-->after printing the synchronous output the its output can be printed
setTimeout(()=>{
    console.log("get the data from the database");
   // p.innerText='Get the data from database';
},5000);
console.log("50");

//p.innerText="Loading...";
console.log("100");
