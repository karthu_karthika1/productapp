import { ComponentFixture, TestBed } from '@angular/core/testing';

import { deleteproductComponent } from './deleteproduct.component';

describe('deleteproductComponent', () => {
  let component: deleteproductComponent;
  let fixture: ComponentFixture<deleteproductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ deleteproductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(deleteproductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
