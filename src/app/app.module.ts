import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { ListproductComponent } from './listproduct/listproduct.component';
import { SearchproductComponent } from './searchproduct/searchproduct.component';
import { HttpClientModule} from '@angular/common/http';
import { ProductService } from './service/product.service';
import { ReactiveFormsModule } from '@angular/forms';
import { UpdateproductComponent } from './updateproduct/updateproduct.component';
import { deleteproductComponent } from './deleteproduct/deleteproduct.component';


@NgModule({
  declarations: [
    AppComponent,
    AddproductComponent,
    ListproductComponent,
    SearchproductComponent,
    UpdateproductComponent,
    deleteproductComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [ProductService],
  bootstrap: [AppComponent],
  
})
export class AppModule { }
